//Marina Martin-Pérez González
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int main (int argc,char *argv[]){
	char buffer[100];
	int tuberia;
	int dev_read;

	mkfifo("/tmp/tuberia",0666);
	tuberia=open("/tmp/tuberia",O_RDONLY);
	if(tuberia==-1){
		perror(":error al abrir la tuberia");
		exit(1);
	}
	while(1){
		dev_read=read(tuberia,&buffer,sizeof(buffer));
		if(dev_read==-1 || buffer[0]=='0'){
			printf("Juego cerrado\n");
			break;
		}

		else	printf("%s",buffer);

	}

	close(tuberia);
	unlink("/tmp/fifo");
	printf("tuberia cerrada y borrada \n ");
return 0;

}
