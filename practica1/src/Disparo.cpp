#include "Disparo.h"
#include "glut.h"

void Disparo::Dibuja(){
	glColor3f(0.0f,1.0f,1.0f);
	glPushMatrix();
	glTranslatef(origen.x,posicion.y,0);
	glutSolidSphere(radio,20,20);
	glPopMatrix();
}
Disparo::Disparo(){
	radio=0.15f;
	velocidad.y=15;
	aceleracion.y=aceleracion.x=0;
}
void Disparo::Mueve(float t){
	posicion=posicion+velocidad*t+aceleracion*(0.5*t*t);
	velocidad=velocidad+aceleracion*t;
}
void Disparo::SetPos(Vector2D pos){
	posicion=pos;
	origen=pos;
}

Vector2D Disparo::getPos(){
	return posicion;
}
float Disparo::getRadio(){
	return radio;
}
Vector2D Disparo::getOrigen(){
	return origen;
}
