#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/mman.h>
#include "DatosMemCompartida.h"

int main (){

	DatosMemCompartida *pdatos;
	char * proyFichero;
	int fichero=open("/tmp/fdatosM",O_RDWR);	

	if(fichero<0){
		perror("Error de apertura de fichero de datosM\n");
		return -1;
	}

	proyFichero=(char*)mmap(NULL,sizeof(pdatos),PROT_WRITE|PROT_READ,MAP_SHARED,fichero,0);

	if(proyFichero==MAP_FAILED){
		perror("Error en la proyeccion del fichero de datosM\n");
		return -1;
	}

	close(fichero); //cerramos el fichero, no necesario para trabajar

	pdatos=(DatosMemCompartida *) proyFichero;

	while(1){
		float posRaqueta;
		posRaqueta=0.5*pdatos->raqueta1.y1+0.5*pdatos->raqueta1.y2;

		if(posRaqueta < pdatos->esfera.centro.y) pdatos->accion=1;
		else if(posRaqueta > pdatos->esfera.centro.y) pdatos->accion=-1;
		else pdatos->accion=0;

		usleep(25000);
	}

	munmap(proyFichero,sizeof(pdatos));
	return 0;
}
